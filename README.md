# ReactJs - The Complete Guide

## Section 6: Styling React Components

Chapter by chapter, [React - The Complete Guide](https://www.udemy.com/course/react-the-complete-guide-incl-redux/) course @udemy

#### notes:

[styled components](https://styled-components.com/) - package that help us build components which have certain styles attached to them, where the styles only affect to the components they are attached (default behaviour is not-component-scope-oriented, so they would apply styles to the whole page)
